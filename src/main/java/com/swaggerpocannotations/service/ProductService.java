package com.swaggerpocannotations.service;


import com.swaggerpocannotations.domain.Product;

public interface ProductService {
    Iterable<Product> listAllProducts();

    Product getProductById(Integer id);

    Product saveProduct(Product product);

}
