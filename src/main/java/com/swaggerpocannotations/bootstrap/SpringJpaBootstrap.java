package com.swaggerpocannotations.bootstrap;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.swaggerpocannotations.domain.Product;
import com.swaggerpocannotations.repository.ProductRepository;

@Component
public class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ProductRepository productRepository;



    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        loadProducts();
    }

    private void loadProducts() {
        Product product = new Product();
        product.setDescription("New product");
        product.setPrice(new BigDecimal("18.95"));
        product.setProductId("235268845711068308");
        productRepository.save(product);


        Product oldProduct = new Product();
        oldProduct.setDescription("Old product");
        oldProduct.setProductId("168639393495335947");
        oldProduct.setPrice(new BigDecimal("11.95"));
        productRepository.save(oldProduct);

    }


    }



